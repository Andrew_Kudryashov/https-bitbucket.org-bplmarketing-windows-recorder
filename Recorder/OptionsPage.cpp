// COptionsPage.cpp : implementation file
//

#include "stdafx.h"
#include "Recorder.h"
#include "OptionsPage.h"
#include "afxdialogex.h"


// CoptionsPage dialog

IMPLEMENT_DYNAMIC(COptionsPage, CPropertyPage)

COptionsPage::COptionsPage()
	: CPropertyPage(IDD_PROPPAGE_OPTIONS)
{

}

COptionsPage::~COptionsPage()
{
}

void COptionsPage::DoDataExchange(CDataExchange* pDX)
{
	CPropertyPage::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_COMBO_QUALITY, m_ComboCtrl);
}


BEGIN_MESSAGE_MAP(COptionsPage, CPropertyPage)
	ON_EN_CHANGE(IDC_EDIT_PATH_FOR_FILES, &COptionsPage::OnEnChangeEditPathForFiles)
END_MESSAGE_MAP()


// CoptionsPage message handlers


void COptionsPage::OnEnChangeEditPathForFiles()
{
	// TODO:  If this is a RICHEDIT control, the control will not
	// send this notification unless you override the CPropertyPage::OnInitDialog()
	// function and call CRichEditCtrl().SetEventMask()
	// with the ENM_CHANGE flag ORed into the mask.

	// TODO:  Add your control notification handler code here
}
