#include "stdafx.h"

#include "AudioSourcesPage.h"
#include "VideoSourcesPage.h"
#include "ScreensPage.h"
#include "OptionsPage.h"


#include "RecorderPropertySheet.h"


IMPLEMENT_DYNAMIC(CRecorderPropertySheet, CPropertySheet)

CRecorderPropertySheet::CRecorderPropertySheet(UINT nIDCaption, CWnd* pParentWnd, UINT iSelectPage)
	:CPropertySheet(nIDCaption, pParentWnd, iSelectPage)
{
	AddPage(&m_VideoPage);
	AddPage(&m_AudioPage);
	AddPage(&m_ScreensPage);
	AddPage(&m_OptionsPage);
}

CRecorderPropertySheet::CRecorderPropertySheet(LPCTSTR pszCaption, CWnd* pParentWnd, UINT iSelectPage)
	:CPropertySheet(pszCaption, pParentWnd, iSelectPage)
{
	AddPage(&m_VideoPage);
	AddPage(&m_AudioPage);
	AddPage(&m_ScreensPage);
	AddPage(&m_OptionsPage);
}

CRecorderPropertySheet::~CRecorderPropertySheet()
{
}


BEGIN_MESSAGE_MAP(CRecorderPropertySheet, CPropertySheet)
	ON_COMMAND(IDCANCEL, &CRecorderPropertySheet::OnIdcancel)
	ON_COMMAND(IDOK, &CRecorderPropertySheet::OnIdok)
END_MESSAGE_MAP()


BOOL CRecorderPropertySheet::OnInitDialog()
{
	BOOL bResult;

	m_bModeless = FALSE;
	m_nFlags |= WF_CONTINUEMODAL;

	bResult = CPropertySheet::OnInitDialog();

	m_bModeless = TRUE;
	m_nFlags &= ~WF_CONTINUEMODAL;
	

	return bResult;
}


void CRecorderPropertySheet::OnIdcancel()
{
	
}


void CRecorderPropertySheet::OnIdok()
{
	// TODO: Add your command handler code here
}
