#pragma once

#include "MonitorProps.h"

class CScreenCapture
{
public:
	CScreenCapture(const CMonitorProps& MonitorProps):m_lSample(0) { m_MonitorProps = MonitorProps; }
	~CScreenCapture();

	BOOL Initialize();

	static UINT CaptureThread(LPVOID pScreenData);
	BOOL StartCapture();
	BOOL StopCapture();
	CString	GetFileName();
	void ReleaseMemory();



private:
	CMonitorProps	m_MonitorProps;
	HANDLE			m_hHeap;
	LPVOID			m_lpBits;
	PAVIFILE		m_pAviFile;
	PAVISTREAM		m_pAviStream;
	HDC				m_hDC;
	LONG			m_lSample;

	CEvent			m_StopEvent;
};

