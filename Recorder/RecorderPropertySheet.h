#pragma once
#include "afxdlgs.h"

#include "AudioSourcesPage.h"
#include "VideoSourcesPage.h"
#include "ScreensPage.h"
#include "OptionsPage.h"

class CVideoSourcesPage;
class CAudioSourcesPage;
class CScreensPage;
class COPtionsPage;

class CRecorderPropertySheet :
	public CPropertySheet
{
	DECLARE_DYNAMIC(CRecorderPropertySheet)

public:
	CRecorderPropertySheet(UINT nIDCaption, CWnd* pParentWnd = NULL, UINT iSelectPage = 0);
	CRecorderPropertySheet(LPCTSTR pszCaption, CWnd* pParentWnd = NULL, UINT iSelectPage = 0);
	virtual ~CRecorderPropertySheet();
public:
	CVideoSourcesPage	m_VideoPage;
	CAudioSourcesPage	m_AudioPage;
	CScreensPage		m_ScreensPage;
	COptionsPage		m_OptionsPage;


protected:
	DECLARE_MESSAGE_MAP()
public:
	virtual BOOL OnInitDialog();
	afx_msg void OnIdcancel();
	afx_msg void OnIdok();
};

