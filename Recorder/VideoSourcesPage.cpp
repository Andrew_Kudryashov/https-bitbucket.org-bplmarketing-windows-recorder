// VideoSourcesPage.cpp : implementation file
//

#include "stdafx.h"
#include "Recorder.h"
#include "VideoCapture.h"

#include "VideoSourcesPage.h"
#include "afxdialogex.h"


// CVideoSourcesPage dialog

IMPLEMENT_DYNAMIC(CVideoSourcesPage, CPropertyPage)

CVideoSourcesPage::CVideoSourcesPage()
	: CPropertyPage(IDD_PROPPAGE_VIDEO_SOURCES)
{

}

CVideoSourcesPage::~CVideoSourcesPage()
{
}

void CVideoSourcesPage::DoDataExchange(CDataExchange* pDX)
{
	CPropertyPage::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_LIST1, m_ListCtrl);
}


BEGIN_MESSAGE_MAP(CVideoSourcesPage, CPropertyPage)
	ON_NOTIFY(LVN_ITEMCHANGED, IDC_LIST1, &CVideoSourcesPage::OnLvnItemchangedList1)
	ON_MESSAGE(WM_START, &CVideoSourcesPage::OnStart)
	ON_MESSAGE(WM_STOP, &CVideoSourcesPage::OnStop)
	ON_WM_TIMER()
END_MESSAGE_MAP()


// CVideoSourcesPage message handlers


void CVideoSourcesPage::OnLvnItemchangedList1(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMLISTVIEW pNMLV = reinterpret_cast<LPNMLISTVIEW>(pNMHDR);

	if (pNMLV->uNewState & LVIS_SELECTED)
	{
		this->SetModified(TRUE);
	}

	if ((pNMLV->uOldState & LVIS_SELECTED)
		&& (pNMLV->uNewState == 0))
	{
		this->SetModified(TRUE);
		m_ListCtrl.SetItemState(pNMLV->iItem, 0, LVIS_SELECTED);
		m_ListCtrl.SetItemState(pNMLV->iItem, 0, LVIS_FOCUSED);
	}

	*pResult = 0;
}


BOOL CVideoSourcesPage::OnApply()
{
	// TODO: Add your specialized code here and/or call the base class
	CArray<int> Temp;
	POSITION pos = this->m_ListCtrl.GetFirstSelectedItemPosition();
	while (NULL != pos)
	{
		int nItem = this->m_ListCtrl.GetNextSelectedItem(pos);
		Temp.Add(nItem);
	}
	// Then we have to compare both arrays: currently selected one and array which was selected on previous iteration
	for (int i = 0; i < m_ListCtrl.GetItemCount(); i++)
	{
		BOOL bWasSelected = IsSelected(i, m_arSelRecords);
		BOOL bIsSelected = IsSelected(i, Temp);

		if (bWasSelected && bIsSelected)
			continue;
		if (bWasSelected)
			PostMessage(WM_STOP, i, 0);
		if (bIsSelected)
			PostMessage(WM_START, i, 0);
	}
	m_arSelRecords.RemoveAll();
	m_arSelRecords.Copy(Temp);
	return CPropertyPage::OnApply();
}

BOOL CVideoSourcesPage::IsSelected(int iRec, const CArray<int>& arSource)
{
	BOOL bResult = FALSE;
	for (int i = 0; i < arSource.GetSize(); i++)
		if (arSource[i] == iRec)
		{
			bResult = TRUE;
			break;
		}
	return bResult;
}

void CVideoSourcesPage::InitRecordList()
{
	m_Devices.Clear();

	HRESULT hr = S_OK;
	TCHAR *szFriendlyName = NULL;

	hr = m_Devices.EnumerateDevices(MF_DEVSOURCE_ATTRIBUTE_SOURCE_TYPE_VIDCAP_GUID);
	LVITEM lv;

	if (SUCCEEDED(hr))
	{
		for (UINT32 iDevice = 0; iDevice < m_Devices.Count(); iDevice++)
		{
			// Get the friendly name of the device.

			hr = m_Devices.GetDeviceName(iDevice, &szFriendlyName);

			if (FAILED(hr))
				break;

			lv.iItem = 0;
			lv.mask = LVIF_TEXT;
			lv.iSubItem = 0;
			lv.pszText = _tcsdup(szFriendlyName);

			this->m_ListCtrl.InsertItem(&lv);

			CoTaskMemFree(szFriendlyName);
			szFriendlyName = NULL;
		}
	}

	InitializeCaptures();
}

void CVideoSourcesPage::InitColumnHeader()
{
	m_ListCtrl.InsertColumn(0, _T("Name"));
	m_ListCtrl.SetColumnWidth(0, 460);
}


void CVideoSourcesPage::InitializeCaptures()
{
	for (UINT32 iDevice = 0; iDevice < m_Devices.Count(); iDevice++)
	{
		CVideoCapture* pCapture = NULL;
		CVideoCapture::CreateInstance(this->m_hWnd, &pCapture);
		m_Captures.Add(pCapture);
	}
}

LRESULT CVideoSourcesPage::OnStart(WPARAM wParam, LPARAM lParam)
{
	IMFActivate* pActivate = NULL;
	m_Devices.GetDevice(wParam, &pActivate);
	CVideoCapture* pCapture = (CVideoCapture*)m_Captures[wParam];
	_TCHAR*  pszDeviceName = NULL;
	m_Devices.GetDeviceName(wParam, &pszDeviceName);
	CString strFileName = pszDeviceName;
	strFileName += _T(" ");

	CTime now = CTime::GetCurrentTime();
	strFileName += now.Format(_T("%Y %m %d %H %M %S"));
	strFileName += _T(".mp4");


	UINT_PTR  pTimer = wParam;
	SetTimer(pTimer, TempTimerValue, NULL);

	pCapture->StartCapture(pActivate, strFileName);

	CoTaskMemFree(pszDeviceName);

	return TRUE;
}

LRESULT CVideoSourcesPage::OnStop(WPARAM wParam, LPARAM lParam)
{
	CVideoCapture* pCapture = (CVideoCapture*)m_Captures[wParam];
	pCapture->EndCaptureSession();
	KillTimer(wParam);

	return TRUE;
}

BOOL CVideoSourcesPage::OnInitDialog()
{
	CPropertyPage::OnInitDialog();

	InitColumnHeader();

	InitRecordList();

	return TRUE;  // return TRUE unless you set the focus to a control
				  // EXCEPTION: OCX Property Pages should return FALSE
}



/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//    class CVideoDeviceList
//
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void CVideoDeviceList::Clear()
{
	for (UINT32 i = 0; i < m_cDevices; i++)
	{
		m_ppDevices[i]->Release();
	}
	CoTaskMemFree(m_ppDevices);
	m_ppDevices = NULL;

	m_cDevices = 0;
}

HRESULT CVideoDeviceList::EnumerateDevices(const GUID& Value)
{
	HRESULT hr = S_OK;
	IMFAttributes *pAttributes = NULL;

	Clear();

	// Initialize an attribute store. We will use this to 
	// specify the enumeration parameters.

	hr = MFCreateAttributes(&pAttributes, 1);

	// Ask for source type = video capture devices
	if (SUCCEEDED(hr))
	{
		hr = pAttributes->SetGUID(
			MF_DEVSOURCE_ATTRIBUTE_SOURCE_TYPE,
			Value
		);
	}

	// Enumerate devices.
	if (SUCCEEDED(hr))
	{
		hr = MFEnumDeviceSources(pAttributes, &m_ppDevices, &m_cDevices);
	}

	pAttributes->Release();

	return hr;
}


HRESULT CVideoDeviceList::GetDevice(UINT32 index, IMFActivate **ppActivate)
{
	if (index >= Count())
	{
		return E_INVALIDARG;
	}

	*ppActivate = m_ppDevices[index];
	(*ppActivate)->AddRef();

	return S_OK;
}

HRESULT CVideoDeviceList::GetDeviceName(UINT32 index, WCHAR **ppszName)
{
	if (index >= Count())
	{
		return E_INVALIDARG;
	}

	HRESULT hr = S_OK;

	hr = m_ppDevices[index]->GetAllocatedString(
		MF_DEVSOURCE_ATTRIBUTE_FRIENDLY_NAME,
		ppszName,
		NULL
	);

	return hr;
}




void CVideoSourcesPage::OnTimer(UINT_PTR nIDEvent)
{
	((CVideoCapture*)(m_Captures[nIDEvent]))->EndCaptureSession();
	PostMessage(WM_START, nIDEvent);

	CPropertyPage::OnTimer(nIDEvent);
}
