// AuidioSourcesPage.cpp : implementation file
//

#include "stdafx.h"
#include "Recorder.h"
#include "AudioSourcesPage.h"
#include "afxdialogex.h"


// CAuidioSourcesPage dialog

IMPLEMENT_DYNAMIC(CAudioSourcesPage, CPropertyPage)

CAudioSourcesPage::CAudioSourcesPage()
	: CPropertyPage(IDD_PROPPAGE_AUDIO_SOURCES)

{

}

CAudioSourcesPage::~CAudioSourcesPage()
{
}

void CAudioSourcesPage::DoDataExchange(CDataExchange* pDX)
{
	CPropertyPage::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_LIST1, m_ListCtrl);
}


BEGIN_MESSAGE_MAP(CAudioSourcesPage, CPropertyPage)
END_MESSAGE_MAP()


// CAuidioSourcesPage message handlers

void CAudioSourcesPage::OnLvnItemchangedList1(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMLISTVIEW pNMLV = reinterpret_cast<LPNMLISTVIEW>(pNMHDR);

	if (pNMLV->uNewState & LVIS_SELECTED)
	{
		this->SetModified(TRUE);
	}

	if ((pNMLV->uOldState & LVIS_SELECTED)
		&& (pNMLV->uNewState == 0))
	{
		this->SetModified(TRUE);
		m_ListCtrl.SetItemState(pNMLV->iItem, 0, LVIS_SELECTED);
		m_ListCtrl.SetItemState(pNMLV->iItem, 0, LVIS_FOCUSED);
	}

	*pResult = 0;
}


BOOL CAudioSourcesPage::OnApply()
{
	// TODO: Add your specialized code here and/or call the base class
	CArray<int> Temp;
	POSITION pos = this->m_ListCtrl.GetFirstSelectedItemPosition();
	while (NULL != pos)
	{
		int nItem = this->m_ListCtrl.GetNextSelectedItem(pos);
		Temp.Add(nItem);
	}
	// Then we have to compare both arrays: currently selected one and array which was selected on previous iteration
	for (int i = 0; i < m_ListCtrl.GetItemCount(); i++)
	{
		BOOL bWasSelected = IsSelected(i, m_arSelRecords);
		BOOL bIsSelected = IsSelected(i, Temp);

		if (bWasSelected && bIsSelected)
			continue;
		if (bWasSelected)
			PostMessage(WM_STOP, i, 0);
		if (bIsSelected)
			PostMessage(WM_START, i, 0);
	}
	m_arSelRecords.RemoveAll();
	m_arSelRecords.Copy(Temp);
	return CPropertyPage::OnApply();
}

BOOL CAudioSourcesPage::IsSelected(int iRec, const CArray<int>& arSource)
{
	BOOL bResult = FALSE;
	for (int i = 0; i < arSource.GetSize(); i++)
		if (arSource[i] == iRec)
		{
			bResult = TRUE;
			break;
		}
	return bResult;
}

void CAudioSourcesPage::InitRecordList()
{
	m_Devices.Clear();

	HRESULT hr = S_OK;
	LPWSTR szFriendlyName = NULL;

	hr = m_Devices.EnumerateDevices(eCapture);
	LVITEM lv;

	if (SUCCEEDED(hr))
	{
		for (UINT32 iDevice = 0; iDevice < m_Devices.Count(); iDevice++)
		{
			// Get the friendly name of the device.

			hr = m_Devices.GetDeviceFriendlyName(iDevice, &szFriendlyName);

			if (FAILED(hr))
				break;

			lv.iItem = 0;
			lv.mask = LVIF_TEXT;
			lv.iSubItem = 0;
			lv.pszText = _tcsdup(szFriendlyName);

			this->m_ListCtrl.InsertItem(&lv);

			free(szFriendlyName);
			szFriendlyName = NULL;
		}
	}

	InitializeCaptures();
}

void CAudioSourcesPage::InitColumnHeader()
{
	m_ListCtrl.InsertColumn(0, _T("Name"));
	m_ListCtrl.SetColumnWidth(0, 460);
}


void CAudioSourcesPage::InitializeCaptures()
{
	for (UINT32 iDevice = 0; iDevice < m_Devices.Count(); iDevice++)
	{
//		CAudioCapture* pCapture = NULL;
//		CAudioCapture::CreateInstance(this->m_hWnd, &pCapture);
//		m_Captures.Add(pCapture);
	}
}

LRESULT CAudioSourcesPage::OnStart(WPARAM wParam, LPARAM lParam)
{
	IMMDevice* pDevice = NULL;
	m_Devices.GetDevice(wParam, &pDevice);
//	CAudioCapture* pCapture = (CAudioCapture*)m_Captures[wParam];
	_TCHAR*  pszDeviceName = NULL;
	m_Devices.GetDeviceName(wParam, &pszDeviceName);
	CString strFileName = pszDeviceName;
	strFileName += _T(" ");

	CTime now = CTime::GetCurrentTime();
	strFileName += now.Format(_T("%Y %m %d %H %M %S"));
	strFileName += _T(".mp4");

//	pCapture->StartCapture(pActivate, strFileName);

	CoTaskMemFree(pszDeviceName);

	return TRUE;
}

LRESULT CAudioSourcesPage::OnStop(WPARAM wParam, LPARAM lParam)
{
//	CAudioCapture* pCapture = (CAudioCapture*)m_Captures[wParam];
//	pCapture->EndCaptureSession();

	return TRUE;
}


BOOL CAudioSourcesPage::OnInitDialog()
{
	CPropertyPage::OnInitDialog();

	InitColumnHeader();

	InitRecordList();

	return TRUE;  // return TRUE unless you set the focus to a control
				  // EXCEPTION: OCX Property Pages should return FALSE
}

































CAudioDeviceList::CAudioDeviceList() : m_cDevices(0), m_DeviceCollection(NULL)
{
}


CAudioDeviceList::~CAudioDeviceList()
{
	if (m_DeviceCollection != NULL)
		Clear();

	m_DeviceCollection->Release();
}

void    CAudioDeviceList::Clear()
{
	for (UINT32 i = 0; i < m_cDevices; i++)
	{
		IMMDevice** pDevice = NULL;
		HRESULT hr = GetDevice(i, pDevice);
		if (SUCCEEDED(hr))
			(*pDevice)->Release();
	}
	if (m_DeviceCollection ) 
		m_DeviceCollection->Release();
	m_cDevices = 0;
}


HRESULT CAudioDeviceList::EnumerateDevices(EDataFlow  eDeviceFlow)
{
	HRESULT hr = S_OK;

	IMMDeviceEnumerator *deviceEnumerator = NULL;
	
	hr = CoCreateInstance(__uuidof(MMDeviceEnumerator), NULL, CLSCTX_INPROC_SERVER, IID_PPV_ARGS(&deviceEnumerator));
	if (FAILED(hr))
	{
		return hr;
	}

	hr = deviceEnumerator->EnumAudioEndpoints(eDeviceFlow, DEVICE_STATE_ACTIVE, &m_DeviceCollection);
	if (FAILED(hr))
	{
		deviceEnumerator->Release();
		return hr;
	}

	hr = m_DeviceCollection->GetCount(&m_cDevices);
	if (FAILED(hr))
	{
		deviceEnumerator->Release();
		return hr;
	}

	deviceEnumerator->Release();

	return hr;
}

HRESULT CAudioDeviceList::GetDevice(UINT32 index, IMMDevice **ppDevice)
{
	return m_DeviceCollection->Item(index, ppDevice);
}

HRESULT CAudioDeviceList::GetDeviceName(UINT32 index, LPWSTR *ppszName)
{
	IMMDevice* ppDevice = NULL;
	HRESULT hr = GetDevice(index, &ppDevice);

	if (SUCCEEDED(hr))
	{
		hr = ppDevice->GetId(ppszName);
	}
	return hr;
}

HRESULT CAudioDeviceList::GetDeviceFriendlyName(UINT32 index, LPWSTR *ppszName)
{
	HRESULT hr = S_OK;
	LPWSTR Name = NULL;
	IMMDevice* pDevice = NULL;

	hr = GetDevice(index, &pDevice);
	if (FAILED(hr))
		return hr;


	hr = pDevice->GetId(&Name);
	if (FAILED(hr))
	{
		pDevice->Release();
		return hr;
	}

	IPropertyStore *propertyStore;
	hr = pDevice->OpenPropertyStore(STGM_READ, &propertyStore);
	
	if (FAILED(hr))
	{
		pDevice->Release();
		return hr;
	}

	PROPVARIANT friendlyName;
	PropVariantInit(&friendlyName);
	hr = propertyStore->GetValue(PKEY_Device_FriendlyName, &friendlyName);

	propertyStore->Release();

	*ppszName = _tcsdup(friendlyName.pwszVal);

	return hr;
}