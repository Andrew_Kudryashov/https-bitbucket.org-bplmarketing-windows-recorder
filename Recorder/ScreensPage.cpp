// ScreensPage.cpp : implementation file
//

#include "stdafx.h"
#include "Recorder.h"
#include "ScreensPage.h"
#include "MonitorProps.h"
#include "afxdialogex.h"

BOOL CALLBACK AddMonitorsCallBack(
	HMONITOR hMonitor,  // handle to display monitor
	HDC hdcMonitor,     // handle to monitor DC
	LPRECT lprcMonitor, // monitor intersection rectangle
	LPARAM dwData       // data
);


// CScreensPage dialog

IMPLEMENT_DYNAMIC(CScreensPage, CPropertyPage)

CScreensPage::CScreensPage()
	: CPropertyPage(IDD_PROPPAGE_SCREENS)
{

}

CScreensPage::~CScreensPage()
{
}

void CScreensPage::DoDataExchange(CDataExchange* pDX)
{
	CPropertyPage::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_LIST1, m_ListCtrl);
}


BEGIN_MESSAGE_MAP(CScreensPage, CPropertyPage)
	ON_NOTIFY(LVN_ITEMCHANGED, IDC_LIST1, &CScreensPage::OnLvnItemchangedList1)
	ON_MESSAGE(WM_START, &CScreensPage::OnStart)
	ON_MESSAGE(WM_STOP, &CScreensPage::OnStop)
END_MESSAGE_MAP()


// CScreensPage message handlers

void CScreensPage::OnLvnItemchangedList1(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMLISTVIEW pNMLV = reinterpret_cast<LPNMLISTVIEW>(pNMHDR);

	if (pNMLV->uNewState & LVIS_SELECTED)
	{
		this->SetModified(TRUE);
	}

	if ((pNMLV->uOldState & LVIS_SELECTED)
		&& (pNMLV->uNewState == 0))
	{
		this->SetModified(TRUE);
		m_ListCtrl.SetItemState(pNMLV->iItem, 0, LVIS_SELECTED);
		m_ListCtrl.SetItemState(pNMLV->iItem, 0, LVIS_FOCUSED);
	}

	*pResult = 0;
}


BOOL CScreensPage::OnApply()
{
	// TODO: Add your specialized code here and/or call the base class
	CArray<int> Temp;
	POSITION pos = this->m_ListCtrl.GetFirstSelectedItemPosition();
	while (NULL != pos)
	{
		int nItem = this->m_ListCtrl.GetNextSelectedItem(pos);
		Temp.Add(nItem);
	}
	// Then we have to compare both arrays: currently selected one and array which was selected on previous iteration
	for (int i = 0; i < m_ListCtrl.GetItemCount(); i++)
	{
		BOOL bWasSelected = IsSelected(i, m_arSelRecords);
		BOOL bIsSelected = IsSelected(i, Temp);

		if (bWasSelected && bIsSelected)
			continue;
		if (bWasSelected)
			PostMessage(WM_STOP, i, 0);
		if (bIsSelected)
			PostMessage(WM_START, i, 0);
	}
	m_arSelRecords.RemoveAll();
	m_arSelRecords.Copy(Temp);
	return CPropertyPage::OnApply();
}

BOOL CScreensPage::IsSelected(int iRec, const CArray<int>& arSource)
{
	BOOL bResult = FALSE;
	for (int i = 0; i < arSource.GetSize(); i++)
		if (arSource[i] == iRec)
		{
			bResult = TRUE;
			break;
		}
	return bResult;
}

LRESULT CScreensPage::OnStart(WPARAM wParam, LPARAM lParam)
{
	IMMDevice* pDevice = NULL;
//	m_Devices.GetDevice(wParam, &pDevice);
	//	CAudioCapture* pCapture = (CAudioCapture*)m_Captures[wParam];
	_TCHAR*  pszDeviceName = NULL;
//	m_Devices.GetDeviceName(wParam, &pszDeviceName);
	CString strFileName = pszDeviceName;
	strFileName += _T(" ");

	CTime now = CTime::GetCurrentTime();
	strFileName += now.Format(_T("%Y %m %d %H %M %S"));
	strFileName += _T(".mp4");

	//	pCapture->StartCapture(pActivate, strFileName);

	CoTaskMemFree(pszDeviceName);

	return TRUE;
}

LRESULT CScreensPage::OnStop(WPARAM wParam, LPARAM lParam)
{
	//	CAudioCapture* pCapture = (CAudioCapture*)m_Captures[wParam];
	//	pCapture->EndCaptureSession();

	return TRUE;
}


void CScreensPage::InitColumnHeader()
{
	this->m_ListCtrl.InsertColumn(0, _T("Name"));
	this->m_ListCtrl.SetColumnWidth(0, 360);
	this->m_ListCtrl.InsertColumn(1, _T("Primary"));
	this->m_ListCtrl.SetColumnWidth(1, 100);

	this->m_ListCtrl.InsertColumn(2, _T("Width"));
	this->m_ListCtrl.SetColumnWidth(2, 240);
	this->m_ListCtrl.InsertColumn(3, _T("Height"));
	this->m_ListCtrl.SetColumnWidth(3, 240);
}

void CScreensPage::InitRecordList()
{
	EnumDisplayMonitors(NULL, NULL, AddMonitorsCallBack, (LPARAM)&this->m_ObArMonitors);

	LVITEM lv;
	for (int i = 0; i < m_ObArMonitors.GetSize(); i++)
	{
		CMonitorProps* pProps = (CMonitorProps*)m_ObArMonitors[i];

		lv.iItem = 0;
		lv.mask = LVIF_TEXT;
		lv.iSubItem = 0;
		lv.pszText = _tcsdup(pProps->m_strDevName.GetString());

		this->m_ListCtrl.InsertItem(&lv);

		lv.iSubItem = 1;
		lv.pszText = (pProps->m_bPrimary ? _T("P") : _T("S"));
		this->m_ListCtrl.SetItem(&lv);

		lv.iSubItem = 2;
		_TCHAR buf[100];
		buf[99] = _T('\0');
		_stprintf_s(buf, 100, _T("%d"), pProps->m_DisplayWidth);
		lv.pszText = _tcsdup(buf);
		this->m_ListCtrl.SetItem(&lv);

		lv.iSubItem = 3;
		buf[99] = _T('\0');
		_stprintf_s(buf, 100, _T("%d"), pProps->m_DisplayHeight);
		lv.pszText = _tcsdup(buf);
		this->m_ListCtrl.SetItem(&lv);
	}
}

BOOL CALLBACK AddMonitorsCallBack(
	HMONITOR hMonitor,  // handle to display monitor
	HDC hdcMonitor,     // handle to monitor DC
	LPRECT lprcMonitor, // monitor intersection rectangle
	LPARAM dwData       // data
)
{
	CObArray* pObArray = (CObArray*)dwData;

	MONITORINFOEX MonitorInfoEx;
	MonitorInfoEx.cbSize = sizeof(MONITORINFOEX);
	GetMonitorInfo(hMonitor, &MonitorInfoEx);

	DISPLAY_DEVICE DisplayDevice;
	ZeroMemory(&DisplayDevice, sizeof(DisplayDevice));
	DisplayDevice.cb = sizeof(DisplayDevice);

	EnumDisplayDevices(MonitorInfoEx.szDevice, 0, &DisplayDevice, 0);

	DEVMODE DevMode;
	ZeroMemory(&DevMode, sizeof(DEVMODE));
	DevMode.dmSize = sizeof(DEVMODE);

	DevMode.dmFields = DM_PELSWIDTH | DM_PELSHEIGHT | DM_BITSPERPEL | DM_DISPLAYFREQUENCY;

	EnumDisplaySettings(
		MonitorInfoEx.szDevice,
		ENUM_CURRENT_SETTINGS,
		&DevMode
	);

	CMonitorProps* pMonitorProps = new CMonitorProps(DisplayDevice.DeviceString, DevMode.dmPelsWidth, DevMode.dmPelsHeight, DevMode.dmBitsPerPel, DevMode.dmDisplayFrequency, hMonitor, MonitorInfoEx.dwFlags & MONITORINFOF_PRIMARY);
	pObArray->Add(pMonitorProps);
	return TRUE;
}

BOOL CScreensPage::OnInitDialog()
{
	CPropertyPage::OnInitDialog();

	InitColumnHeader();

	InitRecordList();

	return TRUE;  // return TRUE unless you set the focus to a control
				  // EXCEPTION: OCX Property Pages should return FALSE
}