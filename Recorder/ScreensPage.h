#pragma once
#include "afxcmn.h"


// CScreensPage dialog

class CScreensPage : public CPropertyPage
{
	DECLARE_DYNAMIC(CScreensPage)

public:
	CScreensPage();
	virtual ~CScreensPage();

// Dialog Data
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_PROPPAGE_SCREENS };
#endif

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
	afx_msg void OnLvnItemchangedList1(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg LRESULT OnStart(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnStop(WPARAM wParam, LPARAM lParam);

	virtual BOOL OnApply();

protected:
	CArray<int>		m_arSelRecords;
	CObArray		m_ObArMonitors;
	BOOL			m_bModified;

	void InitColumnHeader();
	void InitRecordList();
	BOOL IsSelected(int iRec, const CArray<int>& arSource);

public:
	
	virtual BOOL OnInitDialog();
	CListCtrl m_ListCtrl;
};
