#include "stdafx.h"
#include "ScreenCapture.h"

#define FPS 1 // temporary constant , have to be set in Options window. 


CScreenCapture::~CScreenCapture()
{
}

UINT CScreenCapture::CaptureThread(LPVOID pScreenData)
{
	CScreenCapture* pCapture = (CScreenCapture*)pScreenData;

	HDC hCompatibleDC = CreateCompatibleDC(pCapture->m_hDC);
	if (hCompatibleDC == NULL)
		return 0;

	UINT uiImageSize = pCapture->m_MonitorProps.m_BitsPerPel*pCapture->m_MonitorProps.m_DisplayHeight*pCapture->m_MonitorProps.m_DisplayWidth / 8;

	BITMAPINFO	bmpInfo;
	ZeroMemory(&bmpInfo, sizeof(BITMAPINFO));
	bmpInfo.bmiHeader.biSize = sizeof(BITMAPINFOHEADER);

	bmpInfo.bmiHeader.biBitCount = pCapture->m_MonitorProps.m_BitsPerPel;
	bmpInfo.bmiHeader.biCompression = BI_RGB;
	bmpInfo.bmiHeader.biWidth = pCapture->m_MonitorProps.m_DisplayWidth;
	bmpInfo.bmiHeader.biHeight = pCapture->m_MonitorProps.m_DisplayHeight;
	bmpInfo.bmiHeader.biPlanes = 1;
	bmpInfo.bmiHeader.biSizeImage = uiImageSize;

	HANDLE hCompatibleBitmap = CreateDIBSection(pCapture->m_hDC, &bmpInfo, DIB_RGB_COLORS, &pCapture->m_lpBits, NULL, 0);
	if (hCompatibleBitmap == NULL)
		return 0;

	SelectObject(hCompatibleDC, hCompatibleBitmap);

	while (TRUE)
	{	
		DWORD dwRes = WaitForSingleObject(pCapture->m_StopEvent, 0);
		if (dwRes == WAIT_OBJECT_0)
			break;
		BOOL bRes = BitBlt(hCompatibleDC, 0, 0, pCapture->m_MonitorProps.m_DisplayWidth, pCapture->m_MonitorProps.m_DisplayHeight, pCapture->m_hDC, 0, 0, SRCCOPY | CAPTUREBLT);
		if (!bRes)
			break;

		HRESULT hr = AVIStreamWrite(pCapture->m_pAviStream, pCapture->m_lSample++, 1, pCapture->m_lpBits, uiImageSize, 0, NULL, NULL);
		if (FAILED(hr))
			break;
		Sleep(1000);
	}
}

BOOL CScreenCapture::Initialize()
{
	m_hDC = CreateDC(_T("DISPLAY"), m_MonitorProps.m_strDevName, NULL, NULL);
	//HDC hDC = CreateDC( m_MonitorProps.m_strDevName, NULL, NULL, NULL);

	if (NULL == m_hDC)
		return FALSE;

	UINT uiHeapSize = m_MonitorProps.m_DisplayHeight*m_MonitorProps.m_DisplayWidth*m_MonitorProps.m_BitsPerPel / 8;

	m_hHeap = HeapCreate(HEAP_NO_SERIALIZE, uiHeapSize, 0);
	if (m_hHeap == NULL) 
		return FALSE;

	m_lpBits = HeapAlloc(m_hHeap, HEAP_ZERO_MEMORY | HEAP_NO_SERIALIZE, uiHeapSize);
	if (m_lpBits == NULL)
		return FALSE;

	HRESULT hr = AVIFileOpen(&m_pAviFile, GetFileName(), OF_CREATE | OF_WRITE, NULL);
	if (FAILED(hr))
		return FALSE;

	AVISTREAMINFO AviStreamInfo;
	ZeroMemory(&AviStreamInfo, sizeof(AVISTREAMINFO));
	AviStreamInfo.fccType = streamtypeVIDEO;
	AviStreamInfo.fccHandler = VIDEOCODEC;
	AviStreamInfo.dwScale = 1;
	AviStreamInfo.dwRate = FPS;		
	AviStreamInfo.dwQuality = -1;	//Default Quality . it's temporary. It has to be set in Options window
	AviStreamInfo.dwSuggestedBufferSize = uiHeapSize;
	SetRect(&AviStreamInfo.rcFrame, 0, 0, m_MonitorProps.m_DisplayWidth, m_MonitorProps.m_DisplayHeight);
	_tcscpy_s(AviStreamInfo.szName, 64, _T("Video Stream"));

	hr = AVIFileCreateStream(m_pAviFile, &m_pAviStream, &AviStreamInfo);

	if (FAILED(hr))
	{
		return FALSE;
	}

	return TRUE;
}

void CScreenCapture::ReleaseMemory()
{
	if (m_hDC)
	{
		DeleteDC(m_hDC);
		m_hDC = NULL;
	}
	if (m_pAviStream)
	{
		AVIStreamRelease(m_pAviStream);
		m_pAviStream = NULL;
	}
	if (m_pAviFile)
	{
		AVIFileRelease(m_pAviFile);
		m_pAviFile = NULL;
	}
	if (m_lpBits)
	{
		HeapFree(m_hHeap, HEAP_NO_SERIALIZE, m_lpBits);
		m_lpBits = NULL;
	}
	if (m_hHeap)
	{
		HeapDestroy(m_hHeap);
		m_hHeap = NULL;
	}
}


CString	CScreenCapture::GetFileName()
{
	CString strFileName = m_MonitorProps.m_strDevName;
	strFileName += _T(" ");

	CTime now = CTime::GetCurrentTime();
	strFileName += now.Format(_T("%Y %m %d %H %M %S"));
	strFileName += _T(".avi");

	return strFileName;
}

BOOL CScreenCapture::StartCapture()
{

	AfxBeginThread(CScreenCapture::CaptureThread, (LPVOID)this);

	return TRUE;


}

BOOL CScreenCapture::StopCapture()
{
	m_StopEvent.SetEvent();
	return FALSE;
}
