#pragma once

const UINT32 bitrate = 240000U;
const GUID   subtype = MFVideoFormat_H264;


class CVideoCapture : public IMFSourceReaderCallback
{
public:
	static HRESULT CreateInstance(
		HWND     hwnd,
		CVideoCapture **ppPlayer
	);

	// IUnknown methods
	STDMETHODIMP QueryInterface(REFIID iid, void** ppv);
	STDMETHODIMP_(ULONG) AddRef();
	STDMETHODIMP_(ULONG) Release();

	// IMFSourceReaderCallback methods
	STDMETHODIMP OnReadSample(
		HRESULT hrStatus,
		DWORD dwStreamIndex,
		DWORD dwStreamFlags,
		LONGLONG llTimestamp,
		IMFSample *pSample
	);

	STDMETHODIMP OnEvent(DWORD, IMFMediaEvent *)
	{
		return S_OK;
	}

	STDMETHODIMP OnFlush(DWORD)
	{
		return S_OK;
	}

	HRESULT     StartCapture(IMFActivate *pActivate, const WCHAR *pwszFileName );
	HRESULT     EndCaptureSession();
	BOOL        IsCapturing();
	HRESULT     CheckDeviceLost(DEV_BROADCAST_HDR *pHdr, BOOL *pbDeviceLost);

protected:

	// Constructor is private. Use static CreateInstance method to instantiate.
	CVideoCapture(HWND hwnd);

	// Destructor is private. Caller should call Release.
	virtual ~CVideoCapture();

	void    NotifyError(HRESULT hr) { PostMessage(m_hwndEvent, WM_APP_PREVIEW_ERROR, (WPARAM)hr, 0L); }

	HRESULT OpenMediaSource(IMFMediaSource *pSource);
	HRESULT ConfigureCapture();

	long                    m_nRefCount;        // Reference count.
	CRITICAL_SECTION        m_critsec;

	HWND                    m_hwndEvent;        // Application window to receive events. 

	IMFSourceReader         *m_pReader;
	IMFSinkWriter           *m_pWriter;

	BOOL                    m_bFirstSample;
	LONGLONG                m_llBaseTime;

	WCHAR                   *m_pwszSymbolicLink;
};

HRESULT ConfigureSourceReader(IMFSourceReader *pReader);
HRESULT ConfigureEncoder(IMFMediaType *pType, IMFSinkWriter *pWriter, DWORD *pdwStreamIndex);
HRESULT CopyAttribute(IMFAttributes *pSrc, IMFAttributes *pDest, const GUID& key);