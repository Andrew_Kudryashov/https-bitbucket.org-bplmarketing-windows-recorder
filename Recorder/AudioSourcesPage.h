#pragma once
#include "afxcmn.h"


// CAuidioSourcesPage dialog

class CAudioDeviceList
{
public:
	CAudioDeviceList();
	~CAudioDeviceList();

	UINT32  Count() const { return m_cDevices; }

	void    Clear();
	HRESULT EnumerateDevices(EDataFlow  eDeviceFlow);
	HRESULT GetDevice(UINT32 index, IMMDevice **ppDevice);
	HRESULT GetDeviceName(UINT32 index, LPWSTR *ppszName);
	HRESULT GetDeviceFriendlyName(UINT32 index, LPWSTR *ppszName);

private:
	IMMDeviceCollection*	m_DeviceCollection;
	UINT32					m_cDevices;
};


class CAudioSourcesPage : public CPropertyPage
{
	DECLARE_DYNAMIC(CAudioSourcesPage)

public:
	CAudioSourcesPage();
	virtual ~CAudioSourcesPage();

// Dialog Data
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_PROPPAGE_AUDIO_SOURCES };
#endif

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()

	afx_msg void OnLvnItemchangedList1(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg LRESULT OnStart(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnStop(WPARAM wParam, LPARAM lParam);

	virtual BOOL OnApply();

protected:
	BOOL IsSelected(int iRec, const CArray<int>& arSource);
	void InitRecordList();
	void InitializeCaptures();
	void InitColumnHeader();

	CArray<int>		m_arSelRecords;
	CPtrArray		m_Captures;
	CAudioDeviceList		m_Devices;
public:
	virtual BOOL OnInitDialog();

public:
	CListCtrl m_ListCtrl;
};
