//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by Recorder.rc
//
#define IDM_ABOUTBOX                    0x0010
#define IDD_ABOUTBOX                    100
#define IDS_ABOUTBOX                    101
#define IDD_PROPPAGE_AUDIO_SOURCES      106
#define IDD_PROPPAGE_VIDEO_SOURCES      108
#define IDD_PROPPAGE_SCREENS            109
#define IDD_PROPPAGE_OPTIONS            110
#define IDR_MAINFRAME                   128
#define IDC_LIST1                       1000
#define IDC_COMBO_QUALITY               1002
#define IDC_SPIN_TIME_INTERVAL          1003
#define IDC_EDIT_TIME_INTERVAL          1004
#define IDC_EDIT_PATH_FOR_FILES         1005
#define IDC_FILE_DIALOG_BUTTON          1006

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        136
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1007
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
