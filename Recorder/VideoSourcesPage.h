#pragma once
#include "afxcmn.h"


// CVideoSourcesPage dialog

const UINT TempTimerValue = 30000; // 

class CVideoDeviceList
{
	UINT32      m_cDevices;
	IMFActivate **m_ppDevices;

public:
	CVideoDeviceList() : m_ppDevices(NULL), m_cDevices(0)
	{

	}
	~CVideoDeviceList()
	{
		Clear();
	}

	UINT32  Count() const { return m_cDevices; }

	void    Clear();
	HRESULT EnumerateDevices(const GUID& Value);
	HRESULT GetDevice(UINT32 index, IMFActivate **ppActivate);
	HRESULT GetDeviceName(UINT32 index, WCHAR **ppszName);
};

class CVideoSourcesPage : public CPropertyPage
{
	DECLARE_DYNAMIC(CVideoSourcesPage)

public:
	CVideoSourcesPage();
	virtual ~CVideoSourcesPage();

// Dialog Data
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_PROPPAGE_VIDEO_SOURCES };
#endif

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	

	DECLARE_MESSAGE_MAP()
public:
	CListCtrl m_ListCtrl;
	afx_msg void OnLvnItemchangedList1(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg LRESULT OnStart(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnStop(WPARAM wParam, LPARAM lParam);

	virtual BOOL OnApply();

protected:
	BOOL IsSelected(int iRec, const CArray<int>& arSource);
	void InitRecordList();
	void InitializeCaptures();
	void InitColumnHeader();

	CArray<int>		m_arSelRecords;
	CPtrArray		m_Captures;
	CVideoDeviceList		m_Devices;
	CArray<UINT_PTR> m_CaptureTimers;
	
public:
	virtual BOOL OnInitDialog();
	afx_msg void OnTimer(UINT_PTR nIDEvent);
};
