#pragma once

class CMonitorProps : public CObject
{
public:
	CMonitorProps() {};
	CMonitorProps(LPTSTR DevName, DWORD dwDisplayWidth, DWORD dwDisplayHeight, DWORD dwBitsPerPel, DWORD dwDisplayFrequency, HMONITOR hMonitor, BOOL bPrimary) : m_strDevName(DevName), m_DisplayWidth(dwDisplayWidth), m_DisplayHeight(dwDisplayHeight), m_BitsPerPel(dwBitsPerPel),
		m_DisplayFrequency(dwDisplayFrequency), m_hMonitor(hMonitor), m_bPrimary(bPrimary)
	{}
	CMonitorProps& operator= (const CMonitorProps& Source)
	{
		m_DisplayWidth = Source.m_DisplayWidth;
		m_DisplayHeight = Source.m_DisplayHeight;
		m_BitsPerPel = Source.m_BitsPerPel;
		m_DisplayFrequency = Source.m_DisplayFrequency;
		m_hMonitor = Source.m_hMonitor;
		m_strDevName = Source.m_strDevName;
		m_bPrimary = Source.m_bPrimary;
	}
	CMonitorProps(const CMonitorProps& Source)
	{
		m_DisplayWidth = Source.m_DisplayWidth;
		m_DisplayHeight = Source.m_DisplayHeight;
		m_BitsPerPel = Source.m_BitsPerPel;
		m_DisplayFrequency = Source.m_DisplayFrequency;
		m_hMonitor = Source.m_hMonitor;
		m_strDevName = Source.m_strDevName;
		m_bPrimary = Source.m_bPrimary;
	}

public:
	DWORD m_DisplayWidth;
	DWORD m_DisplayHeight;
	DWORD m_BitsPerPel;
	DWORD m_DisplayFrequency;
	HMONITOR m_hMonitor;
	CString m_strDevName;
	BOOL m_bPrimary;
};
