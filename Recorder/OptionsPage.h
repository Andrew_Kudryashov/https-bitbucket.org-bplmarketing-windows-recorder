#pragma once
#include "afxwin.h"


// CoptionsPage dialog

class COptionsPage : public CPropertyPage
{
	DECLARE_DYNAMIC(COptionsPage)

public:
	COptionsPage();
	virtual ~COptionsPage();

// Dialog Data
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_PROPPAGE_OPTIONS };
#endif

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
	CComboBox m_ComboCtrl;
	afx_msg void OnEnChangeEditPathForFiles();
};
